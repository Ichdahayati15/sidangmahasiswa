﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel;

namespace SidangMahasiswa.MVC.Controllers
{
    public class JadwalSIdangController : Controller
    {
        // GET: JadwalSIdang
        public ActionResult Index(string searchString, int? page, string currentFilter)
        {
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            List<JadwalSidangViewModel> listJad = new List<JadwalSidangViewModel>();
            listJad = JadwalSidangRepo.All(searchString);

            if (!String.IsNullOrEmpty(searchString))
            {
                listJad = listJad.Where(a => a.Mahasiswa_Nama.ToUpper().Contains(searchString.ToUpper())).ToList();
                if (listJad.Count() != 0)
                {
                    TempData["tidakAda"] = null;
                }
                else
                {
                    TempData["tidakAda"] = "Data tidak ditemukan";
                }
            }
            else
            {
                listJad = JadwalSidangRepo.All(searchString);
            }

            return View();
        }

        public ActionResult List()
        {
            return PartialView("_List", JadwalSidangRepo.AllList()) ;
        }

        public ActionResult Create()
        {
            ViewBag.MahasiswaList = new SelectList(MahasiswaRepo.All(), "Id", "Nama");
            ViewBag.TugasAkhirList = new SelectList(TugasAkhirRepo.ByMahasiswa(0), "Id", "Pembimbing", "Judul");
            return PartialView("_Create", new JadwalSidangViewModel());

        }

        public ActionResult GetByMahasiswa(long mahasiswaId)
        {
            return View(TugasAkhirRepo.ByMahasiswa(mahasiswaId));
        }

        [HttpPost]
        public ActionResult Create(JadwalSidangViewModel model)
        {

            ResponseResult result = JadwalSidangRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult Edit(int id)
        {
            JadwalSidangViewModel model = JadwalSidangRepo.ById(id);
            ViewBag.MahasiswaList = new SelectList(MahasiswaRepo.All(), "Id", "Nama");
            ViewBag.TugasAkhirList = new SelectList(TugasAkhirRepo.ByMahasiswa(model.MahasiswaId), "Id", "Pembimbing", "Judul");
            return PartialView("_Edit", model);
        }

        [HttpPost]
        public ActionResult Edit(JadwalSidangViewModel model)
        {
            ResponseResult result = JadwalSidangRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int id)
        {
            return PartialView("_Delete", JadwalSidangRepo.ById(id));
        }

        [HttpPost]
        public ActionResult Delete(JadwalSidangViewModel model)
        {
            ResponseResult result = JadwalSidangRepo.Delete(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }

    }
}