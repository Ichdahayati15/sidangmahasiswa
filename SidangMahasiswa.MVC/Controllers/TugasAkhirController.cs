﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel;

namespace SidangMahasiswa.MVC.Controllers
{
    public class TugasAkhirController : Controller
    {
        // GET: TugasAkhir
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult List()
        {
            return PartialView("_List", TugasAkhirRepo.All());
        }

        public ActionResult Create()
        {
            ViewBag.MahasiswaList = new SelectList(TugasAkhirRepo.AllMahasiswa(), "Id", "Nama");
            return PartialView("_Create", new TugasAkhirViewModel());
        }

        [HttpPost]
        public ActionResult Create(TugasAkhirViewModel model)
        {

            ResponseResult result = TugasAkhirRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult Edit(int id)
        {
            TugasAkhirViewModel model = TugasAkhirRepo.ById(id);
            ViewBag.MahasiswaList = new SelectList(TugasAkhirRepo.AllMahasiswa(), "Id", "Nama");
            return PartialView("_Edit", model);
        }

        [HttpPost]
        public ActionResult Edit(TugasAkhirViewModel model)
        {
            ResponseResult result = TugasAkhirRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int id)
        {
            return PartialView("_Delete", TugasAkhirRepo.ById(id));
        }

        [HttpPost]
        public ActionResult Delete(TugasAkhirViewModel model)
        {
            ResponseResult result = TugasAkhirRepo.Delete(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }
    }
}