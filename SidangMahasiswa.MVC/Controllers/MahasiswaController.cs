﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel;

namespace SidangMahasiswa.MVC.Controllers
{
    public class MahasiswaController : Controller
    {
        // GET: Mahasiswa
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult List()
        {
            return PartialView("_List", MahasiswaRepo.All());
        }
        public ActionResult Create()
        {
            return PartialView("_Create", new MahasiswaViewModel());
        }
        [HttpPost]
        public ActionResult Create(MahasiswaViewModel model)
        {
            ResponseResult result = MahasiswaRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Edit(int id)
        {
            return PartialView("_Edit", MahasiswaRepo.ById(id));
        }

        [HttpPost]
        public ActionResult Edit(MahasiswaViewModel model)
        {
            ResponseResult result = MahasiswaRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Delete(int id)
        {
            return PartialView("Delete", MahasiswaRepo.ById(id));
        }
        [HttpPost]
        public ActionResult Delete(MahasiswaViewModel model)
        {
            ResponseResult result = MahasiswaRepo.Delete(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }
    }
}