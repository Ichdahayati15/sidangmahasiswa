﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel;

namespace DataAccess
{
    public class MahasiswaRepo
    {
        //Get All
        public static List<MahasiswaViewModel> All()
        {
            List<MahasiswaViewModel> result = new List<MahasiswaViewModel>();
            using (var db = new Context())
            {
                result = (from m in db.Mahasiswas
                          where m.is_deleted == false
                          select new MahasiswaViewModel
                          {
                              Id = m.Id,
                              Nama = m.Nama,
                              Nim = m.Nim
                          }).ToList();
            }
            return result;
        }

        //Get by Id
        public static MahasiswaViewModel ById(int id)
        {
            MahasiswaViewModel result = new MahasiswaViewModel();
            using (var db = new Context())
            {
                result = (from m in db.Mahasiswas
                          where m.Id == id
                          select new MahasiswaViewModel
                          {
                              Id = m.Id,
                              Nama = m.Nama,
                              Nim = m.Nim
                          }).FirstOrDefault();
            }
            return result != null ? result : new MahasiswaViewModel();
        }

        //Create New & Edit
        public static ResponseResult Update(MahasiswaViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new Context())
                {
                    #region Create New
                    if(entity.Id == 0)
                    {
                        Mahasiswa mahasiswa = new Mahasiswa();

                        mahasiswa.Nama = entity.Nama;
                        mahasiswa.Nim = entity.Nim;
                        mahasiswa.is_deleted = false;
                        mahasiswa.CreatedBy = "ami";
                        mahasiswa.CreatedOn = DateTime.Now;

                        db.Mahasiswas.Add(mahasiswa);
                        db.SaveChanges();
                        result.Entity = entity;

                    }
                    #endregion
                    #region Edit
                    else
                    {
                        Mahasiswa mahasiswa = db.Mahasiswas
                            .Where(o => o.Id == entity.Id)
                            .FirstOrDefault();
                        if(mahasiswa != null)
                        {
                            mahasiswa.Nama = entity.Nama;
                            mahasiswa.Nim = entity.Nim;

                            mahasiswa.ModifiedBy = "ami";
                            mahasiswa.ModifiedOn = DateTime.Now;

                            db.SaveChanges();

                            result.Entity = entity;

                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "Mahasiswa not found!";
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }

        //Delete
        public static ResponseResult Delete(MahasiswaViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using(var db = new Context())
                {
                    Mahasiswa mahasiswa = db.Mahasiswas
                        .Where(o => o.Id == entity.Id)
                        .FirstOrDefault();
                    if(mahasiswa != null)
                    {
                        mahasiswa.deletedBy = 1;
                        mahasiswa.deletedOn = DateTime.Now;

                        mahasiswa.is_deleted = true;
                        db.SaveChanges();

                        result.Entity = entity;
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Mahasiswa not found!";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
    }
}
