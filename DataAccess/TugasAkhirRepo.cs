﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel;

namespace DataAccess
{
    public class TugasAkhirRepo
    {
        
        //Get All
        public static List<TugasAkhirViewModel> All()
        {
            List<TugasAkhirViewModel> result = new List<TugasAkhirViewModel>();
            using (var db = new Context())
            {
                result = (from t in db.TugasAkhirs
                          join m in db.Mahasiswas
                          on t.MahasiswaId equals m.Id
                          where t.is_deleted == false
                          select new TugasAkhirViewModel
                          {
                              MahasiswaId = t.MahasiswaId,
                              Nama = m.Nama,
                              Id = t.Id,
                              Kategori = t.Kategori,
                              Judul = t.Judul,
                              Pembimbing = t.Pembimbing
                          }).ToList();
            }
            return result;
        }

        //Get by Id
        public static TugasAkhirViewModel ById(int id)
        {
            TugasAkhirViewModel result = new TugasAkhirViewModel();
            using (var db = new Context())
            {
                result = (from t in db.TugasAkhirs
                          join m in db.Mahasiswas
                          on t.MahasiswaId equals m.Id
                          where t.Id == id
                          select new TugasAkhirViewModel
                          {
                              MahasiswaId = t.MahasiswaId,
                              Nama = m.Nama,
                              Id = t.Id,
                              Kategori = t.Kategori,
                              Judul = t.Judul,
                              Pembimbing = t.Pembimbing
                          }).FirstOrDefault();
            }
            return result != null ? result : new TugasAkhirViewModel();
        }

        public static List<MahasiswaViewModel> AllMahasiswa()
        {
            List<MahasiswaViewModel> result = new List<MahasiswaViewModel>();
            using (var db = new Context())
            {
                result = (from m in db.Mahasiswas
                          select new MahasiswaViewModel
                          {
                              Id = m.Id,
                              Nama = m.Nama,
                              Nim = m.Nim
                          }).ToList();
            }
            return result;
        }
        public static List<TugasAkhirViewModel> ByMahasiswa(long MahasiswaId)
        {
            List<TugasAkhirViewModel> result = new List<TugasAkhirViewModel>();
            using (var db = new Context())
            {
                result = (from t in db.TugasAkhirs
                          join m in db.Mahasiswas
                          on t.MahasiswaId equals m.Id
                          where t.MahasiswaId == MahasiswaId
                          select new TugasAkhirViewModel
                          {
                              MahasiswaId = m.Id,
                              Id = t.Id,
                              Kategori = t.Kategori,
                              Judul = t.Judul,
                              Pembimbing = t.Pembimbing
                          }).ToList();
            }
            return result;
        }

        //Create New & Edit
        public static ResponseResult Update(TugasAkhirViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new Context())
                {
                    #region Create New
                    if (entity.Id == 0)
                    {
                        TugasAkhir tugasAkhir = new TugasAkhir();

                        tugasAkhir.MahasiswaId = entity.MahasiswaId;
                        tugasAkhir.Kategori = entity.Kategori;
                        tugasAkhir.Judul = entity.Judul;
                        tugasAkhir.Pembimbing = entity.Pembimbing;

                        tugasAkhir.is_deleted = false;
                        tugasAkhir.CreatedBy = "ami";
                        tugasAkhir.CreatedOn = DateTime.Now;

                        db.TugasAkhirs.Add(tugasAkhir);
                        db.SaveChanges();
                        result.Entity = entity;

                    }
                    #endregion
                    #region Edit
                    else
                    {
                        TugasAkhir tugasAkhir = db.TugasAkhirs
                            .Where(o => o.Id == entity.Id)
                            .FirstOrDefault();
                        if (tugasAkhir != null)
                        {
                            tugasAkhir.MahasiswaId = entity.MahasiswaId;
                            tugasAkhir.Kategori = entity.Kategori;
                            tugasAkhir.Judul = entity.Judul;
                            tugasAkhir.Pembimbing = entity.Pembimbing;

                            tugasAkhir.ModifiedBy = "ami";
                            tugasAkhir.ModifiedOn = DateTime.Now;

                            db.SaveChanges();
                            result.Entity = entity;

                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "TugasAkhir not found!";
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }

        //Delete
        public static ResponseResult Delete(TugasAkhirViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new Context())
                {
                    TugasAkhir tugasAkhir = db.TugasAkhirs
                        .Where(o => o.Id == entity.Id)
                        .FirstOrDefault();
                    if (tugasAkhir != null)
                    {
                        tugasAkhir.deletedBy = 1;
                        tugasAkhir.deletedOn = DateTime.Now;

                        tugasAkhir.is_deleted = true;
                        db.SaveChanges();
                        result.Entity = entity;
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "TugasAkhir not found!";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
    }
}
