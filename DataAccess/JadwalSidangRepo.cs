﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel;

namespace DataAccess
{
    public class JadwalSidangRepo
    {
        //Get All
        public static List<JadwalSidangViewModel> All(string searchString)
        {
            List<JadwalSidangViewModel> result = new List<JadwalSidangViewModel>();
            using (var db = new Context())
            {
                result = (from j in db.JadwalSidangs
                          join t in db.TugasAkhirs
                          on j.TugasAkhirId equals t.Id
                          join m in db.Mahasiswas
                          on j.MahasiswaId equals m.Id
                          where j.is_deleted == false
                          select new JadwalSidangViewModel
                          {
                              Id = j.Id,
                              TugasAkhirId = j.TugasAkhirId,
                              MahasiswaId = j.MahasiswaId,
                              Tanggal = j.Tanggal,
                              Penguji = j.Penguji,
                              Mahasiswa_Nama = m.Nama,
                              Pembimbing = t.Pembimbing,
                              Judul = t.Judul

                          }).ToList();
            }
            return result;
        }
        public static List<JadwalSidangViewModel> AllList()
        {
            List<JadwalSidangViewModel> result = new List<JadwalSidangViewModel>();
            using (var db = new Context())
            {
                result = (from j in db.JadwalSidangs
                          join t in db.TugasAkhirs
                          on j.TugasAkhirId equals t.Id
                          join m in db.Mahasiswas
                          on j.MahasiswaId equals m.Id
                          where j.is_deleted == false
                          select new JadwalSidangViewModel
                          {
                              Id = j.Id,
                              TugasAkhirId = j.TugasAkhirId,
                              MahasiswaId = j.MahasiswaId,
                              Tanggal = j.Tanggal,
                              Penguji = j.Penguji,
                              Mahasiswa_Nama = m.Nama,
                              Pembimbing = t.Pembimbing,
                              Judul = t.Judul

                          }).ToList();
            }
            return result;
        }
        public static JadwalSidangViewModel ById(int id)
        {
            JadwalSidangViewModel result = new JadwalSidangViewModel();
            using (var db = new Context())
            {
                result = (from j in db.JadwalSidangs
                          join t in db.TugasAkhirs
                          on j.TugasAkhirId equals t.Id
                          join m in db.Mahasiswas
                          on j.MahasiswaId equals m.Id
                          where j.is_deleted == false
                          && j.MahasiswaId == id
                          select new JadwalSidangViewModel
                          {
                              Id = j.Id,
                              TugasAkhirId = j.TugasAkhirId,
                              MahasiswaId = j.MahasiswaId,
                              Tanggal = j.Tanggal,
                              Penguji = j.Penguji,
                              Mahasiswa_Nama = m.Nama,
                              Pembimbing = t.Pembimbing,
                              Judul = t.Judul

                          }).FirstOrDefault();
            }
            return result != null ? result : new JadwalSidangViewModel();
        }

        
        //Create New & Edit
        public static ResponseResult Update(JadwalSidangViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new Context())
                {
                    #region Create New
                    if (entity.Id == 0)
                    {
                        JadwalSidang jadwalSidang = new JadwalSidang();

                        jadwalSidang.MahasiswaId = entity.MahasiswaId;
                        jadwalSidang.TugasAkhirId = entity.TugasAkhirId;
                        jadwalSidang.Tanggal = entity.Tanggal;
                        jadwalSidang.Penguji = entity.Penguji;

                        jadwalSidang.is_deleted = false;
                        jadwalSidang.CreatedBy = "ami";
                        jadwalSidang.CreatedOn = DateTime.Now;

                        db.JadwalSidangs.Add(jadwalSidang);
                        db.SaveChanges();
                        result.Entity = entity;

                    }
                    #endregion
                    #region Edit
                    else
                    {
                        JadwalSidang jadwalSidang = db.JadwalSidangs
                            .Where(o => o.Id == entity.Id)
                            .FirstOrDefault();
                        if (jadwalSidang != null)
                        {
                            jadwalSidang.MahasiswaId = entity.MahasiswaId;
                            jadwalSidang.TugasAkhirId = entity.TugasAkhirId;
                            jadwalSidang.Tanggal = entity.Tanggal;
                            jadwalSidang.Penguji = entity.Penguji;

                            jadwalSidang.ModifiedBy = "ami";
                            jadwalSidang.ModifiedOn = DateTime.Now;

                            db.SaveChanges();
                            result.Entity = entity;

                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "Jadwal Sidang not found!";
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }

        //Delete
        public static ResponseResult Delete(JadwalSidangViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new Context())
                {
                    JadwalSidang jadwalSidang = db.JadwalSidangs
                        .Where(o => o.Id == entity.Id)
                        .FirstOrDefault();
                    if (jadwalSidang != null)
                    {
                        jadwalSidang.deletedBy = 1;
                        jadwalSidang.deletedOn = DateTime.Now;

                        jadwalSidang.is_deleted = true;
                        db.SaveChanges();
                        result.Entity = entity;
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Jadwal mokika wkwk not found!";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
    }
}
