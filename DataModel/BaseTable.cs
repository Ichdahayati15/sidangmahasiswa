﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public class BaseTable
    {
        [Required, StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }
        
        [StringLength(50)]
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public long? deletedBy { get; set; }

        public DateTime? deletedOn { get; set; }

        public bool is_deleted { get; set; }

    }
}
