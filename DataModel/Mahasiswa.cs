﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    [Table("Mahasiswa")]
    public class Mahasiswa : BaseTable
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Required, StringLength(50)]
        public string Nama { get; set; }

        [StringLength(10)]
        public string Nim { get; set; }

        public virtual ICollection<TugasAkhir> TugasAkhirs { get; set; }
    }
}
