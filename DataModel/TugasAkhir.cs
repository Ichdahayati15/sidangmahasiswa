﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    [Table("TugasAkhir")]
    public class TugasAkhir : BaseTable
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public long MahasiswaId { get; set; }

        [StringLength(10)]
        public string Kategori { get; set; }

        [Required, StringLength(50)]
        public string Pembimbing { get; set; }

        [Required,StringLength(255)]
        public string Judul { get; set; }

        public virtual ICollection<JadwalSidang> JadwalSidangs { get; set; }
    }
}
