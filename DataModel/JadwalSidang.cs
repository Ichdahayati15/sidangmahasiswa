﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    [Table("JadwalSidang")]
    public class JadwalSidang : BaseTable
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public long MahasiswaId { get; set; }

        public long TugasAkhirId { get; set; }

        [Column(TypeName = "date")]
        public DateTime Tanggal { get; set; }

        [Required, StringLength(50)]
        public string Penguji { get; set; }

        public virtual TugasAkhir TugasAkhir { get; set; }
    }
}
