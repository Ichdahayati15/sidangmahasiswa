﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class JadwalSidangViewModel
    {
        public long Id { get; set; }

        public long MahasiswaId { get; set; }

        public long TugasAkhirId { get; set; }

        [Column(TypeName = "date")]
        public DateTime Tanggal { get; set; }

        [Required, StringLength(50)]
        public string Penguji { get; set; }

        public string Mahasiswa_Nama { get; set; }

        public string Pembimbing { get; set; }

        public string Judul { get; set; }
    }
}
