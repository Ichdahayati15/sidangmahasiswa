﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class TugasAkhirViewModel
    {
        public long Id { get; set; }

        public long MahasiswaId { get; set; }

        [StringLength(10)]
        public string Kategori { get; set; }

        [Required, StringLength(50)]
        public string Pembimbing { get; set; }

        [Required, StringLength(255)]
        public string Judul { get; set; }

        public string Nama { get; set; }
    }
}
