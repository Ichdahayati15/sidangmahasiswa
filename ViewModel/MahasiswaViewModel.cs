﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class MahasiswaViewModel
    {
        public long Id { get; set; }

        [Required, StringLength(50)]
        public string Nama { get; set; }

        [StringLength(10)]
        public string Nim { get; set; }
    }
}
